#!/bin/sh

# remove this file
rm -- "$0"

wget https://gitlab.com/tom.pavelec/spine/-/archive/master/spine-master.tar.gz
tar -xzf spine-master.tar.gz
cp -a ./spine-master/. ./
rm -rf spine-master spine-master.tar.gz

#run install
make install
