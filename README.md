# Spine
Spine is a PHP project skeleton. Provides quick setup of project basics. 

## Install
- Run `git clone your-new-project` in the project folder
- Download [install.sh](https://gitlab.com/tom.pavelec/spine/-/raw/master/install.sh?inline=false) to the project folder
- Run `sh install.sh` and follow instructions

### Alternative approach
- Run `git clone your-new-project` in the project folder
- Download [Spine](https://gitlab.com/tom.pavelec/spine/-/archive/master/spine-master.tar.gz) *(somewhere)*
- Extract it to project folder
- Run `make install` and follow instructions

## After Install
- resolve [Checklist](#checklist)
- clear `README.md`

## Checklist

- check encoding (UTF-8)
    - http output (index.php)
        - [ ] contains `mb_http_output('UTF-8');`
        - [ ] returns header `header('Content-Type: text/html; charset=UTF-8')`
    - [ ] bootstrap (index.php, console, every entry point) contains `mb_internal_encoding('UTF-8');`

- [ ] check default time zone (UTC)
- [ ] if a BC break version issued, provide rector rule for library users to smooth upgrade

## Features

### Vendor patches
When there is a need to patch some 3rd party libraries and keep composer updates

- copy desired "file" and add ".new" extension (i.e. "some3rdPartyLibrary.php.new")
- make changes in new file
- run `make generate-vendor-patch` (patch files are generated in ./etc/patch)
- you can add generated patch files to vcs
  - if you do, they are **automatically** applied after all composer updates
