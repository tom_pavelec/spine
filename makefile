
# Reset
NC=\033[0m

# Regular Colors
BLACK=\033[0;30m
RED=\033[0;31m
GREEN=\033[0;32m
YELLOW=\033[0;33m
BLUE=\033[0;34m
PURPLE=\033[0;35m
CYAN=\033[0;36m
WHITE=\033[0;37m

# Bold
BBLACK=\033[1;30m
BRED=\033[1;31m
BGREEN=\033[1;32m
BYELLOW=\033[1;33m
BBLUE=\033[1;34m
BPURPLE=\033[1;35m
BCYAN=\033[1;36m
BWHITE=\033[1;37m

# Underline
UBLACK=\033[4;30m
URED=\033[4;31m
UGREEN=\033[4;32m
UYELLOW=\033[4;33m
UBLUE=\033[4;34m
UPURPLE=\033[4;35m
UCYAN=\033[4;36m
UWHITE=\033[4;37m

# Background
ON_BLACK=\033[40m
ON_RED=\033[41m
ON_GREEN=\033[42m
ON_YELLOW=\033[43m
ON_BLUE=\033[44m
ON_PURPLE=\033[45m
ON_CYAN=\033[46m
ON_WHITE=\033[47m

# High Intensity
IBLACK=\033[0;90m
IRED=\033[0;91m
IGREEN=\033[0;92m
IYELLOW=\033[0;93m
IBLUE=\033[0;94m
IPURPLE=\033[0;95m
ICYAN=\033[0;96m
IWHITE=\033[0;97m

TARGETS_TEST=

#<<<INSTALL
TARGETS_TEST_FILE=./bin/build/test_targets.txt

## Install
.PHONY: install
install:
	@printf "${ICYAN}${ON_BLACK} -> Cleaning unwanted files ${NC}\n"
	@rm -rf src/.gitkeep
	@rm -rf install.sh

	@printf "${ICYAN}${ON_BLACK} -> Project settings ${NC}\n"
	@printf "Enter ${RED}project type${NC} ${BLUE}app / lib${NC} (lib): "; \
	read project_type; \
	if [[ -z "$$project_type" || "$$project_type" == "lib" ]]; \
	then \
	project_type="library"; \
	sed -i -E 's/vendor/vendor\ncomposer.lock/g' .gitignore; \
	else \
	project_type="application"; \
	fi; \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s/:project_type:/$$project_type/g" {} \;

	@printf "Enter ${RED}vendor${NC} name: "; \
	read vendor; \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s#:vendor:#$$vendor#g" {} \;

	@printf "Enter ${RED}package${NC} name: "; \
	read package; \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s#:package_name:#$$package#g" {} \;

	@printf "Enter ${RED}project homepage${NC} (Default): "; \
	read homepage; \
	origin="`git config --get remote.origin.url`"; \
	origin_homepage="`sed -r 's/git@(.+):(.+)\.git/https:\/\/\1\/\2/g' <<<$$origin`" ; \
	if [[ -z "$$homepage" ]]; \
	then \
	homepage="$$origin_homepage"; \
	fi; \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s;:homepage:;$$homepage;g" {} \; && \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s;:origin_homepage:;$$origin_homepage;g" {} \;

	@printf "Enter ${RED}author homepage${NC} (None): "; \
	read author_homepage; \
	if [[ -z "$$author_homepage" ]]; then author_homepage=""; else author_homepage="\\\",\n            \\\"homepage\\\": \\\"$$author_homepage"; fi; \
	find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s;:author_homepage:;$$author_homepage;g" {} \;

	@find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s/:author:/`git config --get user.name`/g" {} \;
	@find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s/:email:/`git config --get user.email`/g" {} \;
	@find ./ -type f ! -name "makefile" ! -path "./.*" -exec sed -i "s/:year:/`date +'%Y'`/g" {} \;

	@printf "${ICYAN}${ON_BLACK} -> Create .env file ${NC}\n"
	@cp .env.dist .env

	@printf "${ICYAN}${ON_BLACK} -> Composer update ${NC}\n"
	@composer update
	@php ./bin/build/update_composer_versions_from_lock.php

	### TESTS
	@printf "${ICYAN}${ON_BLACK} -> Generate Codeception ${NC}\n"
	@php vendor/bin/codecept bootstrap --quiet
	@find ./tests -name ".gitignore" -exec rm -rf {} \+;
	@printf "\n\ncoverage:\n    enabled: true\n    include:\n        - src/*\n" >> codeception.yml;

	@touch ${TARGETS_TEST_FILE}

	@printf "Generate ${RED}unit${NC} tests ${IYELLOW}y${NC}/n: "; \
	read generate_unit; \
	if [[ -z "$$generate_unit" || "$$generate_unit" == "y" ]]; \
	then \
	cp bin/build/bootstrap_bypass_finals.php tests/unit/bootstrap.unit.php; \
	printf "\n\nbootstrap: bootstrap.unit.php\n" >> tests/unit.suite.yml; \
	printf "unit\n" >> ${TARGETS_TEST_FILE};  \
	else find ./tests -regextype sed -regex ".*[uU]nit.*" -exec rm -rf {} \+; \
	fi;

	@printf "Generate ${RED}functional${NC} (integration) tests ${IYELLOW}y${NC}/n: "; \
	read generate_functional; \
	if [[ -z "$$generate_functional" || "$$generate_functional" == "y" ]]; \
	then \
	cp bin/build/bootstrap_bypass_finals.php tests/functional/bootstrap.functional.php; \
	printf "\n\nbootstrap: bootstrap.functional.php\n" >> tests/functional.suite.yml; \
	printf "functional\n" >> ${TARGETS_TEST_FILE}; \
	else find ./tests -regextype sed -regex ".*[fF]unctional.*" -exec rm -rf {} \+; \
	fi;

	@printf "Rename ${RED}functional to integration${NC} tests y/${IYELLOW}n${NC}: "; \
	read rename_functional; \
	if [[ "$$rename_functional" == "y" ]]; \
	then \
	find ./tests -type d -execdir rename functional integration {} \; ; \
	find ./tests -type d -execdir rename Functional Integration {} \; ; \
	find ./tests -type f -exec rename functional integration {} \; ; \
	find ./tests -type f -exec rename Functional Integration {} \; ; \
	find ./tests -type f -exec sed -i 's/functional/integration/g' {} \; ; \
	find ./tests -type f -exec sed -i 's/Functional/Integration/g' {} \; ; \
	sed -i -E 's/functional/integration/g' ${TARGETS_TEST_FILE}; \
	fi;

	@printf "Generate ${RED}acceptance${NC} tests ${IYELLOW}y${NC}/n: "; \
	read generate_acceptance; \
	if [[ -z "$$generate_acceptance" || "$$generate_acceptance" == "y" ]]; \
	then \
	printf "acceptance\n" >> ${TARGETS_TEST_FILE}; \
	else find ./tests -regextype sed -regex ".*[aA]cceptance.*" -exec rm -rf {} \+; \
	fi;

	@php ./bin/build/build_run_test_commands.php ${TARGETS_TEST_FILE}

	### DELETE BUILD FILES
	@printf "${ICYAN}${ON_BLACK} -> Deleting build files ${NC}\n"
	@rm -rf ./bin/build .src/.gitkeep

	### ADD FILES TO GIT
	@printf "${ICYAN}${ON_BLACK} -> Add files to git ${NC}\n"
	@git add etc/* .editorconfig .env.dist .gitignore codeception.yml CHANGELOG.md composer.json LICENSE makefile README.md tests/* bin/*

	### CLEAN INSTALL
	@printf "${ICYAN}${ON_BLACK} -> Cleaning makefile ${NC}\n"
	@perl -i -0pe 's/#<<<INSTALL(\s|.)+?#<<<INSTALL\sEND//g' makefile
#<<<INSTALL END

## COMPOSER ------------------------------------------------------------------------------------------------------------

.PHONY: lockfix
## Fix git* urls in composer.lock (there is no settings yet v1.9.1)
lockfix:
	if [ "$(shell uname)" == "Linux" ]; then sed -i -E 's/git@gitlab([\w\d\.]+):([\w\d\.\/]+)/https:\/\/gitlab\1\/\2/g' composer.lock; fi

.PHONY: outdated
## Show outdated composer packages
outdated:
	composer outdated -D

## PHP CODE STYLE CHECK / FIX ------------------------------------------------------------------------------------------

PHP_CS_SETTINGS=--standard=./etc/ruleset.xml --extensions=php

.PHONY: cs
## Code style (dry run)
cs:
	vendor/bin/phpcs $(PHP_CS_SETTINGS) -sp src $(TARGETS_TEST) --parallel=8

.PHONY: cs-fix
## Fix code style
cs-fix:
	vendor/bin/phpcbf $(PHP_CS_SETTINGS) -sp src $(TARGETS_TEST)

## PHP STAN ------------------------------------------------------------------------------------------------------------

.PHONY: stan-src
## Run PHPStan on /src folder
stan-src:
	php -d memory_limit=256M vendor/bin/phpstan analyse src -c etc/phpstan.neon --level 8

.PHONY: stan-tests
## Run PHPStan on /tests folder
stan-tests:
	php -d memory_limit=256M vendor/bin/phpstan analyse $(TARGETS_TEST) -c etc/phpstan_tests.neon --level 8

.PHONY: stan
## Run PHPStan
stan: stan-src stan-tests

.PHONY: stancc
## Clear PHPStan cache
stancc:
	php vendor/bin/phpstan clear-result-cache

## TEST ----------------------------------------------------------------------------------------------------------------

.PHONY: build-tests
## Build Codeception tests
build-tests:
	php vendor/bin/codecept build

TEST_TEMPLATE=XDEBUG_MODE=coverage php vendor/bin/codecept run
TEST_TEMPLATE_OPTIONS=--coverage-cobertura --verbose

#<<<UNIT_RUNNER
#<<<INTEGRATION_RUNNER
#<<<FUNCTIONAL_RUNNER
#<<<ACCEPT_RUNNER
#<<<TEST_RUNNER

## CI ------------------------------------------------------------------------------------------------------------------

.PHONY: ci
## Run all CI commands
ci: lockfix cs stan test

.PHONY: patch-vendor
## Apply *.patch files for ./vendor folder
patch-vendor:
	sh ./bin/vendor_patcher.sh apply_patch

# copy a vendor file to file.new
# make desired changes in file.new
.PHONY: generate-vendor-patch
## Generate *.patch files from vendor changes
generate-vendor-patch:
	sh ./bin/vendor_patcher.sh create_vendor_patch

# Documentation
.DEFAULT_GOAL := help

TARGET_MAX_CHAR_NUM=22
.SILENT:
.PHONY: help
help:
	@printf "Available commands:\n";
	@awk '{ \
		if ($$0 ~ /^.PHONY: [a-zA-Z\-\\_0-9]+$$/) { \
			helpCommand = substr($$0, index($$0, ":") + 2); \
			if (helpMessage) { \
				cnt=cnt+1; \
				if (cnt%2 == 0) { bg=bgone; } else { bg=bgtwo; } \
				printf " ${BRED}"bg"%-"width"s${NC}${BBLUE}"bg"%s${NC}\n", \
					helpCommand, helpMessage; \
				helpMessage = ""; \
			} \
		} else if ($$0 ~ /^[a-zA-Z\-\\_0-9.]+:/) { \
			helpCommand = substr($$0, 0, index($$0, ":")-1); \
			if (helpMessage) { \
				cnt=cnt+1; \
				if (cnt%2 == 0) { bg=bgone; } else { bg=bgtwo; } \
				printf " ${BRED}"bg"%-"width"s${NC}${BBLUE}"bg"%s${NC}\n", \
					helpCommand, helpMessage; \
				helpMessage = ""; \
			} \
		} else if ($$0 ~ /^##/) { \
			if (helpMessage) { \
				helpMessage = helpMessage "\n"  substr($$0, 3); \
			} else { \
				helpMessage = substr($$0, 3); \
			} \
		} else { \
			if (helpMessage) { \
				print helpMessage; \
				cnt=1; \
			} \
			helpMessage = ""; \
		} \
	}' \
	cnt=1 \
	bgone="\033[48;2;63;50;63m" \
	bgtwo="" \
	width="$$(($$(grep -o '^[a-zA-Z0-9_-]\+:' $(MAKEFILE_LIST) | wc -L)-8))" \
	$(MAKEFILE_LIST)
