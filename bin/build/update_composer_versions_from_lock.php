<?php

$composerFileName = __DIR__ . '/../../composer.json';

$composer = json_decode(
    file_get_contents($composerFileName),
    true,
    512,
    JSON_THROW_ON_ERROR,
);

function updateComposerVersionsFromLock(
    array $tree
): array {
    $result = [];

    foreach ($tree as $package => $requiredVersion) {
        if ($requiredVersion === '*') {
            $packageInfo = shell_exec(
                sprintf(
                    "composer show '%s'",
                    $package,
                )
            );

            preg_match('/versions\s*:\s*\**\s*v*(\d+)\.(\d*)\.(\d*)/', $packageInfo, $matches);

            if (!isset($matches[1])) {
                $result[$package] = $requiredVersion;

                continue;
            }

            $result[$package] = trim(
                sprintf(
                    '~%s.%s',
                    $matches[1],
                    $matches[2],
                ),
                '.',
            );
        } else {
            $result[$package] = $requiredVersion;
        }
    }

    return $result;
}

$composer['require'] = updateComposerVersionsFromLock($composer['require']);
$composer['require-dev'] = updateComposerVersionsFromLock($composer['require-dev']);

echo str_replace(
    '\/',
    '/',
    json_encode($composer, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT)
);
