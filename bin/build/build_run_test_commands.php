<?php

$types = explode(
    "\n",
    trim(
        file_get_contents(
            $argv[1]
        )
    )
);

$filename = __DIR__ . '/../../makefile';

$content = file_get_contents($filename);

$content = str_replace(
    'TARGETS_TEST=',
    sprintf(
        'TARGETS_TEST=%s',
        implode(
            ' ',
            array_map(
                static fn (string $type): string => 'tests/' . $type,
                $types
            )
        )
    ),
    $content
);

function createRunner(string $type, string $content): string {
    $template = <<<TYPE
        .PHONY: $type
        ## Run $type tests
        $type:
        	php $(TEST_TEMPLATE) $type $(TEST_TEMPLATE_OPTIONS)
        	mv tests/_output/cobertura.xml tests/_output/cobertura.$type.xml

        TYPE;

    if ($type === 'accept') {
        $template .= <<<TYPE

            .PHONY: acc-server-up
            acc-server-up:
            	screen -S test_http_server -d -m chromedriver --url-base=/wd/hub

            .PHONY: acc-server-down
            acc-server-down:
            	screen -S test_http_server -X kill

            TYPE;
    }

    return str_replace(
        '#<<<' . strtoupper($type) . '_RUNNER',
        $template,
        $content
    );
}

foreach ($types as $type) {
    $content = createRunner($type, $content);
}

$content = str_replace(
    '#<<<TEST_RUNNER',
    <<<TYPE
    .PHONY: test
    test: $argv[1]
    TYPE,
    $content
);

$content = preg_replace('/#<<<\w+_RUNNER\n/', '', $content);

file_put_contents($filename, $content);
